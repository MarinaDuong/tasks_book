package com.example.lenovo.taskbook.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.example.lenovo.taskbook.entities.SubtaskEntity;
import com.example.lenovo.taskbook.entities.TaskEntity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Lenovo on 14.12.2017.
 */

public class DatabaseAdapter {

    private static DatabaseHelper helper;
    private static DatabaseAdapter instance;
    public static final int DB_ACTION_INSERT_TASK = 1;
    public static final int DB_ACTION_GET_TASKS = 2;
    public static final int DB_ACTION_GET_SUBTASKS = 3;
    public static final int DB_ACTION_INSERT_SUBTASK = 4;
    public static final int DB_ACTION_GET_TASK = 5;
    public static final int DB_ACTION_GET_SUBTASK = 6;
    private String TAG = "DatabaseAdapter";

    public static DatabaseAdapter getInstance(Context ctx) {
        if (instance == null) {
            instance = new DatabaseAdapter(ctx);
        }
        return instance;
    }

    private DatabaseAdapter(Context ctx) {
        helper = DatabaseHelper.getInstance(ctx);
    }

    private long insertTask(String title) {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.TaskTable.COLUMN_TASK_TITLE, title);
        long id = db.insert(DatabaseContract.TaskTable.TABLE_NAME, null, values);
        Log.e(TAG, "!!!!!insertTask id = " + id + ", title = " + title);
        return id;
    }

    private long insertSubtask(String title, long taskId) {
        SQLiteDatabase db = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.SubTasksTable.COLUMN_SUBTASK_TITLE, title);
        values.put(DatabaseContract.SubTasksTable.COLUMN_SUBTASK_TASK_ID, taskId);
        long id = db.insert(DatabaseContract.SubTasksTable.TABLE_NAME, null, values);
        return id;
    }

    private TaskEntity getTask(long taskId) {
        Log.e(TAG, "!!!!!getTask taskId = " + taskId);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + DatabaseContract.TaskTable.TABLE_NAME + " where " + DatabaseContract.TaskTable.COLUMN_TASK_ID + " = " + taskId, null);
        cursor.moveToFirst();
        String title = cursor.getString(cursor.getColumnIndex(DatabaseContract.TaskTable.COLUMN_TASK_TITLE));
        Log.e(TAG, "!!!!!getTask title = " + title);
        TaskEntity task = new TaskEntity(taskId, title);
        cursor.close();
        Log.e(TAG, "!!!!!getTask task = " + task);
        return task;
    }

    private ArrayList<TaskEntity> getTasks() {
        SQLiteDatabase db = helper.getReadableDatabase();
        ArrayList<TaskEntity> list = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from " + DatabaseContract.TaskTable.TABLE_NAME, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String title = cursor.getString(cursor.getColumnIndex(DatabaseContract.TaskTable.COLUMN_TASK_TITLE));
                int id = cursor.getInt(cursor.getColumnIndex(DatabaseContract.TaskTable.COLUMN_TASK_ID));
                TaskEntity task = new TaskEntity(id, title);
                list.add(task);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return list;
    }

    private ArrayList<SubtaskEntity> getSubTasks(long taskId) {
        SQLiteDatabase db = helper.getReadableDatabase();
        ArrayList<SubtaskEntity> list = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from " + DatabaseContract.SubTasksTable.TABLE_NAME + " where " + DatabaseContract.SubTasksTable.COLUMN_SUBTASK_TASK_ID + " = " + taskId, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                long subtaskId = cursor.getInt(cursor.getColumnIndex(DatabaseContract.SubTasksTable.COLUMN_SUBTASK_ID));
                String title = cursor.getString(cursor.getColumnIndex(DatabaseContract.SubTasksTable.COLUMN_SUBTASK_TITLE));
                int isCheck = cursor.getInt(cursor.getColumnIndex(DatabaseContract.SubTasksTable.COLUMN_SUBTASK_IS_CHECK));
                SubtaskEntity task = new SubtaskEntity(subtaskId, taskId, title, isCheck != 0);
                list.add(task);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return list;
    }

    private SubtaskEntity getSubtask(long subtaskId) {
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + DatabaseContract.SubTasksTable.TABLE_NAME + " where " +
                DatabaseContract.SubTasksTable.COLUMN_SUBTASK_ID + " = " + subtaskId, null);
        cursor.moveToFirst();
        long taskId = cursor.getLong(cursor.getColumnIndex(DatabaseContract.SubTasksTable.COLUMN_SUBTASK_TASK_ID));
        String title = cursor.getString(cursor.getColumnIndex(DatabaseContract.SubTasksTable.COLUMN_SUBTASK_TITLE));
        int isChack = cursor.getInt(cursor.getColumnIndex(DatabaseContract.SubTasksTable.COLUMN_SUBTASK_IS_CHECK));
        SubtaskEntity subtask = new SubtaskEntity(subtaskId, taskId, title, isChack != 0);
        return subtask;
    }

    private class DBWorkerTask extends AsyncTask<Object, Void, Object> {

        private WeakReference<ResultListener> mResultReceiver;
        private int actionId;

        public DBWorkerTask(ResultListener pResultReceiver) {
            super();
            this.mResultReceiver = new WeakReference<DatabaseAdapter.ResultListener>(pResultReceiver);
        }

        @Override
        protected Object doInBackground(Object... params) {
            Object result = null;
            actionId = (Integer) params[0];
            long taskId;
            long subtaskId;
            switch (actionId) {

                case DB_ACTION_INSERT_TASK:
                    String task = (String) params[1];
                    result = insertTask(task);
                    break;

                case DB_ACTION_GET_TASKS:
                    result = getTasks();
                    break;

                case DB_ACTION_GET_SUBTASKS:
                    taskId = (long) params[1];
                    result = getSubTasks(taskId);
                    break;

                case DB_ACTION_INSERT_SUBTASK:
                    String subtask = (String) params[1];
                    taskId = (long) params[2];
                    result = insertSubtask(subtask, taskId);
                    break;

                case DB_ACTION_GET_TASK:
                    taskId = (long) params[1];
                    result = getTask(taskId);
                    break;
                case DB_ACTION_GET_SUBTASK:
                    subtaskId = (Long) params[1];
                    result = getSubtask(subtaskId);
                    break;
                default:
                    break;
            }
            return result;
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);

            ResultListener resultReceiver = mResultReceiver.get();
            if (resultReceiver != null) {
                resultReceiver.onDBActionComplited(actionId, result);
            }
        }

    }

    public interface ResultListener {
        public void onDBActionComplited(int actionId, Object result);
    }

    public void insertTask(String title, ResultListener resultReceiver) {
        new DBWorkerTask(resultReceiver).execute(DB_ACTION_INSERT_TASK,
                title);
    }

    public void insertSubtask(String title, long taskId, ResultListener resultReceiver) {
        new DBWorkerTask(resultReceiver).execute(DB_ACTION_INSERT_SUBTASK,
                title, taskId);
    }

    public void getTasks(ResultListener resultReceiver) {
        new DBWorkerTask(resultReceiver).execute(DB_ACTION_GET_TASKS);
    }

    public void getSubtasks(long taskId, ResultListener resultReceiver) {
        new DBWorkerTask(resultReceiver).execute(DB_ACTION_GET_SUBTASKS, taskId);
    }

    public void getTask(long taskId, ResultListener resultReceiver) {
        new DBWorkerTask(resultReceiver).execute(DB_ACTION_GET_TASK, taskId);
    }

    public void getSubtask(long subtaskId, ResultListener resultReceiver) {
        new DBWorkerTask(resultReceiver).execute(DB_ACTION_GET_SUBTASK, subtaskId);
    }
}
