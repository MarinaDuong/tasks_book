package com.example.lenovo.taskbook;

import android.app.Application;

/**
 * Created by Lenovo on 12.12.2017.
 */

public class TaskBookAplication extends Application {

    public static TaskBookAplication getTaskBookAplication() {
        return taskBookAplication;
    }

    private static TaskBookAplication taskBookAplication;

    @Override
    public void onCreate() {
        super.onCreate();

        taskBookAplication = this;
    }
}
