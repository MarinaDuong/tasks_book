package com.example.lenovo.taskbook.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lenovo.taskbook.events.TaskInsertedEvent;
import com.example.lenovo.taskbook.R;
import com.example.lenovo.taskbook.RecyclerViewClickListener;
import com.example.lenovo.taskbook.ui.adapters.TaskAdapter;
import com.example.lenovo.taskbook.entities.TaskEntity;
import com.example.lenovo.taskbook.ui.activities.MainActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

/**
 * Created by Lenovo on 04.01.2018.
 */

public class TaskFragment extends Fragment {
    private OnHeadlineSelectedListener mCallback;
    private ArrayList<TaskEntity> tasks;
    private RecyclerView recyclerView;
    private String TAG = "TaskFragment";
    private TaskAdapter taskAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        MainActivity.keyFragment = MainActivity.KEY_TASKS_PARCEABLE_A_L;
        Bundle bundle = this.getArguments();
        if (bundle == null) {
            tasks = new ArrayList<>();
        } else {
            tasks = bundle.getParcelableArrayList(MainActivity.KEY_TASKS_PARCEABLE_A_L);
        }
        View view = inflater.inflate(R.layout.first_fragment_layout, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        taskAdapter = new TaskAdapter(tasks, new RecyclerViewClickListener() {
            @Override
            public void onRecyclerViewClickListener(View view, int position) {
                long taskId = tasks.get(position).getId();
                Log.e(TAG, "!!!!onCreateView taskId = " + taskId);
                Log.e(TAG, "!!!!onCreateView task title = " + tasks.get(position).getTitle());
                mCallback.onArticleSelected(taskId);
            }
        });
        recyclerView.setAdapter(taskAdapter);
        return view;
    }

    public interface OnHeadlineSelectedListener {
        void onArticleSelected(long taskId);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Проверка в if защищает от ClassCastException - "является ли переменная context экземпляром класса (имплементит интерфейс) OnHeadlineSelectedListener"
        if (context instanceof OnHeadlineSelectedListener) {
            mCallback = (OnHeadlineSelectedListener) context;
        }
    }


    @Subscribe
    public void onTaskInsertedEvent(TaskInsertedEvent taskInsertedEvent) {
        tasks.add(taskInsertedEvent.getTask());
        recyclerView.getAdapter().notifyItemInserted(tasks.size() - 1);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
