package com.example.lenovo.taskbook.ui.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.lenovo.taskbook.events.SubtaskInsertedEvent;
import com.example.lenovo.taskbook.events.TaskInsertedEvent;
import com.example.lenovo.taskbook.R;
import com.example.lenovo.taskbook.ui.fragments.SubtaskFragment;
import com.example.lenovo.taskbook.ui.fragments.TaskFragment;
import com.example.lenovo.taskbook.db.DatabaseAdapter;
import com.example.lenovo.taskbook.entities.SubtaskEntity;
import com.example.lenovo.taskbook.entities.TaskEntity;
import com.truizlop.fabreveallayout.FABRevealLayout;
import com.truizlop.fabreveallayout.OnRevealChangeListener;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        DatabaseAdapter.ResultListener, TaskFragment.OnHeadlineSelectedListener {

    private ArrayList<TaskEntity> tasksNames;
    private DatabaseAdapter databaseAdapter;
    private long taskId;
    private EditText editText;
    public static String KEY_TASKS_PARCEABLE_A_L = "parcelableTasks";
    public static String KEY_SUBTASKS_SERIALIZABLE_A_L = "serializableSubtasks";
    public static String NOTIFY_ITEM_INSERTED = "notifyItemInserted";
    public static String keyFragment;
    private String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        final FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//
//            }
//        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            databaseAdapter = DatabaseAdapter.getInstance(this);
            databaseAdapter.getTasks(this);
        }

        configureFABReveal((FABRevealLayout) findViewById(R.id.fab_reveal_layout));
    }

    private void configureFABReveal(FABRevealLayout fabRevealLayout) {
        fabRevealLayout.setOnRevealChangeListener(new OnRevealChangeListener() {
            @Override
            public void onMainViewAppeared(FABRevealLayout fabRevealLayout, View mainView) {
            }

            @Override
            public void onSecondaryViewAppeared(final FABRevealLayout fabRevealLayout, View secondaryView) {
                (secondaryView.findViewById(R.id.ok_button)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideKeybord();
                        editText = findViewById(R.id.task_name_et);
                        String title = editText.getText().toString();
                        editText.setText(null);
                        if (keyFragment.equals(KEY_TASKS_PARCEABLE_A_L)) {
                            Log.e(TAG, "!!!когда нажимаем кнопку создать task");
                            databaseAdapter.insertTask(title, MainActivity.this);
                        } else {
                            Log.e(TAG, "!!!когда нажимаем кнопку создать subtask     taskId = " + taskId);
                            databaseAdapter.insertSubtask(title, taskId, MainActivity.this);
                        }
                        fabRevealLayout.revealMainView();
                    }
                });
            }
        });
    }

    public void hideKeybord() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDBActionComplited(int actionId, Object result) {
        switch (actionId) {
            case DatabaseAdapter.DB_ACTION_GET_TASKS:
                tasksNames = (ArrayList<TaskEntity>) result;
                TaskFragment taskFragment = new TaskFragment();
                Bundle b = new Bundle();
                b.putParcelableArrayList(KEY_TASKS_PARCEABLE_A_L, tasksNames);
                taskFragment.setArguments(b);
                getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, taskFragment).commit();
                break;
            case DatabaseAdapter.DB_ACTION_INSERT_TASK:
                databaseAdapter.getTask((long) result, MainActivity.this);
                break;
            case DatabaseAdapter.DB_ACTION_GET_TASK:
                EventBus.getDefault().post(new TaskInsertedEvent((TaskEntity) result));
                break;
            case DatabaseAdapter.DB_ACTION_INSERT_SUBTASK:
                databaseAdapter.getSubtask((long) result, MainActivity.this);
                break;
            case DatabaseAdapter.DB_ACTION_GET_SUBTASK:
                EventBus.getDefault().post(new SubtaskInsertedEvent((SubtaskEntity) result));
                break;
            case DatabaseAdapter.DB_ACTION_GET_SUBTASKS:
                ArrayList<SubtaskEntity> subTasks = (ArrayList<SubtaskEntity>) result;
                Bundle bundle = new Bundle();
                bundle.putSerializable(KEY_SUBTASKS_SERIALIZABLE_A_L, subTasks);
                SubtaskFragment subtaskFragment = new SubtaskFragment();
                subtaskFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, subtaskFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
            default:
                break;
        }
    }

    @Override
    public void onArticleSelected(long taskId) {
        databaseAdapter = DatabaseAdapter.getInstance(this);
        databaseAdapter.getSubtasks(taskId, this);
        this.taskId = taskId;
        Log.e(TAG, "!!!когда переходим на второй фрагмент taskId = " + taskId);
    }
}
