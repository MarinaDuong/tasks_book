package com.example.lenovo.taskbook.entities;


import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Lenovo on 12.12.2017.
 */

public class TaskEntity implements Parcelable {
    private long id;
    private String title;
    private String description;

    public TaskEntity() {

    }

    public TaskEntity(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public static final Creator<TaskEntity> CREATOR = new Creator<TaskEntity>() {
        @Override
        public TaskEntity createFromParcel(Parcel parcel) {
            TaskEntity te = new TaskEntity();
            te.setId(parcel.readLong());
            te.setTitle(parcel.readString());
            te.setDescription(parcel.readString());
            return te;
        }

        @Override
        public TaskEntity[] newArray(int size) {
            return new TaskEntity[size];
        }
    };

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(description);
    }
}
