package com.example.lenovo.taskbook.events;

import com.example.lenovo.taskbook.entities.SubtaskEntity;

/**
 * Created by Lenovo on 14.02.2018.
 */

public class SubtaskInsertedEvent {
    private SubtaskEntity subtaskEntity;

    public SubtaskInsertedEvent(SubtaskEntity subtaskEntity) {
        this.subtaskEntity = subtaskEntity;
    }

    public SubtaskEntity getSubtask() {
    return subtaskEntity;
    }
}
