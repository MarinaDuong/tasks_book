package com.example.lenovo.taskbook.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lenovo.taskbook.R;
import com.example.lenovo.taskbook.RecyclerViewClickListener;
import com.example.lenovo.taskbook.entities.TaskEntity;

import java.util.ArrayList;

/**
 * Created by Lenovo on 12.12.2017.
 */

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.MyViewHolder> {

    private ArrayList<TaskEntity> listName;
    private RecyclerViewClickListener recyclerViewClickListener;

    public TaskAdapter(ArrayList<TaskEntity> listName, RecyclerViewClickListener onRecyclerViewClickListener) {
        this.listName = listName;
        this.recyclerViewClickListener = onRecyclerViewClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, description;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.task_name);
            description = view.findViewById(R.id.description);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            recyclerViewClickListener.onRecyclerViewClickListener(view, getLayoutPosition());
        }
    }

    @Override
    public TaskAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_main, parent, false);
        return new MyViewHolder(listRow);
    }

    @Override
    public void onBindViewHolder(TaskAdapter.MyViewHolder holder, int position) {
        TaskEntity taskEntity = listName.get(position);
        holder.title.setText(taskEntity.getTitle());
        holder.description.setText(taskEntity.getDescription());
    }

    @Override
    public int getItemCount() {
        return listName.size();
    }

}
