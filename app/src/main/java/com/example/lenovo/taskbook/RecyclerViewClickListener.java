package com.example.lenovo.taskbook;

import android.view.View;

/**
 * Created by Lenovo on 09.01.2018.
 */

public interface RecyclerViewClickListener {
    void onRecyclerViewClickListener(View view, int position);
}
