package com.example.lenovo.taskbook.db;

import android.provider.BaseColumns;

/**
 * Created by Lenovo on 13.12.2017.
 */

public class DatabaseContract {

    public static abstract class TaskTable implements BaseColumns {
        public static final String TABLE_NAME = "tasks";
        public static final String COLUMN_TASK_ID = "id";
        public static final String COLUMN_TASK_TITLE = "task_title";
    }

    public static abstract class SubTasksTable implements BaseColumns {
        public static final String TABLE_NAME = "subtasks";
        public static final String COLUMN_SUBTASK_ID = "id";
        public static final String COLUMN_SUBTASK_TITLE = "subtask_title";
        public static final String COLUMN_SUBTASK_TASK_ID = "task_id";
        public static final String COLUMN_SUBTASK_IS_CHECK = "is_check";
    }

    private static final String SQL_CREATE_TASK_TABLE = "CREATE TABLE " +
            TaskTable.TABLE_NAME + " (" +
            TaskTable.COLUMN_TASK_ID + " INTEGER PRIMARY KEY," +
            TaskTable.COLUMN_TASK_TITLE + " TEXT)";

    private static final String SQL_DELETE_TASK_TABLE =
            "DROP TABLE IF EXISTS " + TaskTable.TABLE_NAME;

    public static String getSqlCreateTaskTable() {
        return SQL_CREATE_TASK_TABLE;
    }

    public static String getSqlDeleteTaskTable() {
        return SQL_DELETE_TASK_TABLE;
    }

    private static final String SQL_CREATE_SUBTASKS_TABLE = "CREATE TABLE " +
            SubTasksTable.TABLE_NAME + " (" +
            SubTasksTable.COLUMN_SUBTASK_ID + " INTEGER PRIMARY KEY," +
            SubTasksTable.COLUMN_SUBTASK_TITLE + " TEXT," +
            SubTasksTable.COLUMN_SUBTASK_TASK_ID + " INTEGER," +
            SubTasksTable.COLUMN_SUBTASK_IS_CHECK + " INTEGER)";

    private static final String SQL_DELETE_SUBTASKS_TABLE =
            "DROP TABLE IF EXISTS " + SubTasksTable.TABLE_NAME;

    public static String getSqlCreateSubtasksTable() {
        return SQL_CREATE_SUBTASKS_TABLE;
    }

    public static String getSqlDeleteSubtasksTable() {
        return SQL_DELETE_SUBTASKS_TABLE;
    }
}

