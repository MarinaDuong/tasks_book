package com.example.lenovo.taskbook.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lenovo.taskbook.events.SubtaskInsertedEvent;
import com.example.lenovo.taskbook.events.TaskInsertedEvent;
import com.example.lenovo.taskbook.R;
import com.example.lenovo.taskbook.ui.adapters.SubtaskAdapter;
import com.example.lenovo.taskbook.entities.SubtaskEntity;
import com.example.lenovo.taskbook.ui.activities.MainActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

/**
 * Created by Lenovo on 04.01.2018.
 */

public class SubtaskFragment extends Fragment {
    ArrayList<SubtaskEntity> subTasks;
    private RecyclerView recyclerView;
    private String TAG = "SubtaskFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        MainActivity.keyFragment = MainActivity.KEY_SUBTASKS_SERIALIZABLE_A_L;
        View view = inflater.inflate(R.layout.first_fragment_layout, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        Bundle bundle = this.getArguments();
        if (bundle == null) {
            subTasks = new ArrayList<>();
        } else {
            subTasks = (ArrayList<SubtaskEntity>) bundle.getSerializable(MainActivity.KEY_SUBTASKS_SERIALIZABLE_A_L);
        }
        recyclerView.setAdapter(new SubtaskAdapter(subTasks, new SubtaskAdapter.RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {

            }
        }));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Subscribe
    public void onSubtaskInsertedEvent(SubtaskInsertedEvent subtaskInsertedEvent) {
        subTasks.add(subtaskInsertedEvent.getSubtask());
        recyclerView.getAdapter().notifyItemInserted(subTasks.size() - 1);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
