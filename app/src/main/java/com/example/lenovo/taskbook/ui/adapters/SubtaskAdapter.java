package com.example.lenovo.taskbook.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lenovo.taskbook.R;
import com.example.lenovo.taskbook.entities.SubtaskEntity;

import java.util.ArrayList;

/**
 * Created by Lenovo on 04.01.2018.
 */

public class SubtaskAdapter extends RecyclerView.Adapter<SubtaskAdapter.MyViewHolder> {

    private ArrayList<SubtaskEntity> listName;
    private RecyclerViewClickListener recyclerViewClickListener;

    public SubtaskAdapter(ArrayList<SubtaskEntity> listName, RecyclerViewClickListener recyclerViewClickListener) {
        this.listName = listName;
        this.recyclerViewClickListener = recyclerViewClickListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title;

        public MyViewHolder(View view, RecyclerViewClickListener mListener) {
            super(view);
            title = (TextView) view.findViewById(R.id.tv_second_title);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            if (recyclerViewClickListener != null && position != RecyclerView.NO_POSITION) {
                recyclerViewClickListener.onClick(view, getAdapterPosition());


            }

        }
    }

    public interface RecyclerViewClickListener {
        void onClick(View view, int position);
    }

    @Override
    public SubtaskAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listRow = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_second_list, parent, false);
        return new MyViewHolder(listRow, recyclerViewClickListener);
    }

    @Override
    public void onBindViewHolder(SubtaskAdapter.MyViewHolder holder, int position) {
        SubtaskEntity taskEntity = listName.get(position);
        holder.title.setText(taskEntity.getTitle());
    }

    @Override
    public int getItemCount() {
        return listName.size();
    }
}

