package com.example.lenovo.taskbook.events;

import com.example.lenovo.taskbook.entities.TaskEntity;

/**
 * Created by Lenovo on 14.01.2018.
 */

public class TaskInsertedEvent {

    private TaskEntity task;

    public TaskInsertedEvent(TaskEntity task) {
        this.task = task;
    }

    public TaskEntity getTask() {
        return task;
    }
}
