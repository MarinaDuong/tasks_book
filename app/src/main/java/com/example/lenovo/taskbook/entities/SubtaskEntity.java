package com.example.lenovo.taskbook.entities;

/**
 * Created by Lenovo on 04.01.2018.
 */

public class SubtaskEntity {
    private long subtaskId;
    private long taskId;
    private String title;
    private boolean isCheck;

    public SubtaskEntity() {
    }

    public SubtaskEntity(long subtaskId, long taskId, String title, boolean isCheck) {
        this.subtaskId = subtaskId;
        this.taskId = taskId;
        this.title = title;
        this.isCheck = isCheck;
    }

    public long getSubtaskId() {
        return subtaskId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }


}
